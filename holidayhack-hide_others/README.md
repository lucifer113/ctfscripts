## Hide Others

Script developed for 2019 Sans Holiday Hack Challenge. Will hide other players. 

See: [https://0xdf.gitlab.io/holidayhack2019/13](https://0xdf.gitlab.io/holidayhack2019/13)
